package main

import (
	"fmt"
	"github.com/gdamore/tcell/v2"
	"math/rand"
	"time"
)

const (
	up int = iota + 1
	left
	down
	right
)

type game struct {
	die                    bool
	x, y, apple, direction int
	pos                    []int
	tiles                  []bool
}

func initGame() *game {
	const (
		x, y, apple int = 30, 10, 5
	)

	var (
		pos   []int
		tiles []bool
	)

	pos = []int{0, 1}
	tiles = make([]bool, x*y)
	setTiles(tiles, y/2*x, y/2*x+x, 0)
	setTiles(tiles, x/3, x*y, x-1)

	return &game{
		die:       false,
		x:         x,
		y:         y,
		apple:     apple,
		direction: 0,
		pos:       pos,
		tiles:     tiles,
	}
}

func initScreen() tcell.Screen {
	var (
		s   tcell.Screen
		err error
	)

	s, err = tcell.NewScreen()
	panicIf(err)

	err = s.Init()
	panicIf(err)

	return s
}

func setTiles(tiles []bool, min, max, skip int) {
	for ; min < max; min += skip + 1 {
		tiles[min] = true
	}
}

func panicIf(err error) {
	if err != nil {
		panic(fmt.Errorf("%+v", err))
	}
}

func intIn(a []int, n int) bool {
	var v int

	for _, v = range a {
		if v == n {
			return true
		}
	}
	return false
}

func screenPrintf(s tcell.Screen, x, y int, f string, a ...any) {
	var (
		i int
		r rune
	)

	for i, r = range []rune(fmt.Sprintf(f, a...)) {
		s.SetContent(x+i, y, r, nil, tcell.StyleDefault)
	}
}

func draw(s tcell.Screen, g *game) {
	var (
		x, y, i int
		r       rune
	)

	for x, y, i = 0, 0, 0; y < g.y; {
		switch true {
		case g.apple == i:
			r = 'O'
		case g.pos[len(g.pos)-1] == i:
			r = '^'
		case g.pos[0] == i:
			r = '$'
		case intIn(g.pos, i):
			r = '-'
		case g.tiles[i]:
			r = '*'
		default:
			r = '▓'
		}

		s.SetContent(x, y, r, nil, tcell.StyleDefault)

		i++
		x++
		if x >= g.x {
			x = 0
			y++
		}
	}

	screenPrintf(s, 0, g.y+2, "Score: %d", len(g.pos)-2)
}

func move(g *game) {
	var n int

	switch g.direction {
	case up:
		n = g.pos[len(g.pos)-1] - g.x
		if n < 0 {
			n += g.x * g.y
		}
	case left:
		n = g.pos[len(g.pos)-1] - 1
		if n%g.x == g.x-1 || n == -1 {
			n += g.x
		}
	case down:
		n = g.pos[len(g.pos)-1] + g.x
		if n >= g.x*g.y {
			n -= g.x * g.y
		}
	case right:
		n = g.pos[len(g.pos)-1] + 1
		if n%g.x == 0 {
			n -= g.x
		}
	default:
		return
	}

	if n == g.pos[len(g.pos)-2] {
		switch g.direction {
		case up, left:
			g.direction += 2
		case down, right:
			g.direction -= 2
		}
		move(g)
		return
	}

	if g.tiles[n] || intIn(g.pos[1:], n) {
		g.die = true
		return
	}

	if g.apple == n {
		g.pos = append(g.pos, n)
		moveApple(g)
		return
	}

	g.pos = append(g.pos[1:], n)
}

func moveApple(g *game) {
	var n int

	for {
		rand.Seed(time.Now().UnixNano())

		n = rand.Intn(g.x*g.y - 1)
		if !intIn(g.pos, n) && !g.tiles[n] {
			g.apple = n
			return
		}
	}
}

func handle(s tcell.Screen, g *game, ev tcell.Event) bool {
	var evk *tcell.EventKey

	switch ev.(type) {
	case *tcell.EventResize:
		s.Sync()
	case *tcell.EventKey:
		evk = ev.(*tcell.EventKey)

		if evk.Rune() == 'q' || evk.Key() == tcell.KeyCtrlC {
			return false
		}

		switch evk.Rune() {
		case 'W', 'w':
			g.direction = up
		case 'A', 'a':
			g.direction = left
		case 'S', 's':
			g.direction = down
		case 'D', 'd':
			g.direction = right
		}
	}

	return true
}

func gameLoop(s tcell.Screen, g *game) {
	var (
		ev   tcell.Event
		evch chan tcell.Event
		quit chan struct{}
	)

	evch = make(chan tcell.Event)
	quit = make(chan struct{})

	go s.ChannelEvents(evch, quit)

	for {
		draw(s, g)
		s.Show()

		select {
		case ev = <-evch:
			if !handle(s, g, ev) {
				return
			}
		case <-time.After(time.Second):
		}

		move(g)

		if g.die {
			return
		}
	}
}

func main() {
	var (
		s tcell.Screen
		g *game
	)

	s = initScreen()
	defer func() {
		s.Fini()
		fmt.Print("\033[H\033[2J")
		fmt.Printf("Score: %d\n", len(g.pos)-2)
	}()

	g = initGame()
	gameLoop(s, g)
}
